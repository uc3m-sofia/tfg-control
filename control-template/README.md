# robot-device-examples

Robot Device library use examples.

## Getting started



## Name

Robot Device examples.

## Description

This project show use examples of [robot-device](https://gitlab.com/uc3m-sofia/robot-device-examples) library. More information in the library page.

## Badges


## Visuals


## Installation

Download project and run:

    cd fcontrol-example && ./script/bootstrap

The project includes the library as a submodule, allowing to edit the library and upload changes from the project itself.

## Usage

Folder examples have the source codes showing specific use cases of [robot-device](https://gitlab.com/uc3m-sofia/robot-device-examples).

## Support



## Roadmap

This project will be updated with new examples when available.

## Contributing


## Authors and acknowledgment

## License

## Project status


