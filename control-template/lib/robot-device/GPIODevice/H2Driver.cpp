#include "H2Driver.h"

H2Driver::H2Driver(int new_pinAIN1, int new_pinAIN2, int new_pinSTBY, int new_pinPWM){

    pins.push_back(new_pinAIN1);
    pins.push_back(new_pinAIN2);
    pins.push_back(new_pinSTBY);
    pins.push_back(new_pinPWM);

    for(int i= 0; i< pins.size()-1; i++){
        gpios.push_back(libsoc_gpio_request(pins[i], LS_GPIO_GREEDY));

    }

    for(int i = 0; i < gpios.size(); i++){
        libsoc_gpio_set_direction(gpios[i], OUTPUT);
        libsoc_gpio_set_level(gpios[i],LOW);
    }

    p = libsoc_pwm_request(0,pins[3], LS_PWM_SHARED);
    if (p == NULL) cerr << "Can't open pwm pin number: " << pins[3] << endl;

    //use default period and duty values (1000*1000*1 , 0%) (1 millisecod total time per cycle of which 0 % is active)
    SetPWM(1000*1000*1,0);

}

H2Driver::~H2Driver(){

    for(int i=0; i<pins.size()-1;i++){
        libsoc_gpio_set_level(gpios[i],LOW);
        libsoc_gpio_free(gpios[i]);
    }
    libsoc_pwm_free(p);
}

void H2Driver::SetPWM(double period, double duty){ //Receive duty as percent and convert later

    libsoc_pwm_set_enabled(p,ENABLED);

    libsoc_pwm_set_period(p,period);

    if (duty > 100){
        printf("Duty cycle cannot be higher than period\n");
        exit(EXIT_FAILURE);
    }
    else{
        //Duty must be an integer
        int newduty=(int)(period*duty/100);
        libsoc_pwm_set_duty_cycle(p,newduty);
    }
}

void H2Driver::changeDutyCycle(int duty){
    pwm_enabled test = libsoc_pwm_get_enabled(p);
    if(test != ENABLED){
        printf("Cannot set duty cycle of disabled PWM");
        exit(EXIT_FAILURE);
    }
    else{
        int newduty=(int)(libsoc_pwm_get_period(p)*duty/100);
        libsoc_pwm_set_duty_cycle(p,newduty);
    }
}

void H2Driver::disablePWM(){
    pwm_enabled test = libsoc_pwm_get_enabled(p);

    if(test == ENABLED){
        libsoc_pwm_set_enabled(p,DISABLED);
    }
}

void H2Driver::enablePWM(){
    pwm_enabled test = libsoc_pwm_get_enabled(p);

    if(test == DISABLED){
        libsoc_pwm_set_enabled(p,ENABLED);
    }
}

void H2Driver::setAIN1(gpio_level level){
    libsoc_gpio_set_level(this->gpios[0],level);
}

void H2Driver::setAIN2(gpio_level level){
    libsoc_gpio_set_level(this->gpios[1],level);
}

void H2Driver::enable(){
    //set enable pin high
    libsoc_gpio_set_level(gpios[2],HIGH);
    //set standard bridge direction
    setAIN1(HIGH);
    setAIN2(LOW);
}

void H2Driver::disable(){
    //set enable pin low
    libsoc_gpio_set_level(gpios[2],LOW);
}

long H2Driver::SetThrottle(long nThrottle)
{
    if (nThrottle < 0)
    {
        setAIN1(LOW);
        setAIN2(HIGH);
        changeDutyCycle(abs(nThrottle));
    }
    else
    {
        setAIN1(HIGH);
        setAIN2(LOW);
        changeDutyCycle(abs(nThrottle));
    }
    return 0;
}
