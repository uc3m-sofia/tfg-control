#include "GPIOJetson.h"


//typedef int (*pcallback)(int, char *);
//pcallback callback;

function<int(int,void*)> callbacks[gpioN];
vector<void*> objects(gpioN);


int sA=0;
int argc=1;
char argv[1];
gpio* pin;
uint npin=0;
int status;
//void InterruptHandler(int pin)
int InterruptHandler(void* pinaddress)
{
    pin=(gpio*)pinaddress;
    npin=pin->gpio;

    status = libsoc_gpio_get_level(pin);
//    std::cout << "npin: " << npin << ", status: " << status << endl;

    callbacks[npin](status,objects[npin]);

    return 0;
}

//Not needed
//void OnRiseHandler(int channel)
//{
//    callbacks[pin](argc,objects[channel]);
//}

//void OnFallHandler(int channel)
//{
//    callbacks[pin](argc,objects[channel]);
//}

long GPIOJetson::GetPin(int pin)
{

    //find a fast way to read inputs
//    { // scope for value
//        ifstream value(_SYSFS_ROOT + "/gpio"s + to_string(ch_info.gpio) + "/value"s);
//        int value_read;
//        value >> value_read;
//        return value_read;
//    } // scope ends
    return 0;
}

GPIOJetson::GPIOJetson()
{
    // Pin Setup.
//    setmode(GPIO::BOARD);
    soc_gpios.resize(gpioN);

}

GPIOJetson::~GPIOJetson()
{
    for (int i=0;i<pins.size();i++)
    {
//        cleanup(pins[i]);
            libsoc_gpio_free(soc_gpios[pins[i]]);

    }

}

//old interrupt alternatives
//void GPIOJetson::InterruptBothEdges(long pin, void * callbackAddress )
//void GPIOJetson::InterruptBothEdges(long pin, auto callbackAddress )

//void GPIOJetson::InterruptOnEdge(long pin, int (*callbackAddress)(int, char *) )
void GPIOJetson::InterruptOnEdge(long pin, void* thisptr, function<int(int,void*)> & new_callback )
{

//    callback=reinterpret_cast<pcallback>(callbackAddress);
    callbacks[pin]=new_callback;


    objects[pin]= thisptr;

    soc_gpios[pin] = libsoc_gpio_request(pin, LS_GPIO_SHARED);
//    GPIO::setup(pin, GPIO::IN);


    libsoc_gpio_set_direction(soc_gpios[pin], INPUT);
    int test=libsoc_gpio_get_direction(soc_gpios[pin]);

    if (test != INPUT)
    {
        printf("Failed to set direction of gpio_button\n");
        exit(EXIT_FAILURE);
    }

    libsoc_gpio_set_edge(soc_gpios[pin], gpio_edge::BOTH);
    libsoc_gpio_callback_interrupt(soc_gpios[pin], &InterruptHandler, (void *) soc_gpios[pin]);
//    GPIO::add_event_detect(pin, GPIO::Edge::BOTH, InterruptHandler, 0);

    pins.push_back(pin);

    //find an alternative for quick reading the values libgpiod??
//    try {
//        ChannelInfo ch_info = _channel_to_info(channel, true);

//        Directions app_cfg = _app_channel_configuration(ch_info);

//        if (app_cfg != IN && app_cfg != OUT)
//            throw runtime_error("You must setup() the GPIO channel first");

//        { // scope for value
//            ifstream value(_SYSFS_ROOT + "/gpio"s + to_string(ch_info.gpio) + "/value"s);
//            int value_read;
//            value >> value_read;
//            return value_read;
//        } // scope ends
//    } catch (exception& e) {
//        _cleanup_all();
//        throw _error(e, "GPIO::input()");
//    }


}

//void GPIOJetson::InterruptOnRise(long pin, void *thisptr, function<int (int, void *)> &new_callback)
//{

//    callbacks[pin]=new_callback;
//    objects[pin]= thisptr;

//    GPIO::setup(pin, GPIO::IN);

//    GPIO::add_event_detect(pin, GPIO::Edge::RISING, InterruptHandler, 0);

//    pins.push_back(pin);

//}

//void GPIOJetson::InterruptOnFall(long pin, void *thisptr, function<int (int, void *)> &new_callback)
//{

//    callbacks[pin]=new_callback;
//    objects[pin]= thisptr;

//    GPIO::setup(pin, GPIO::IN);

//    GPIO::add_event_detect(pin, GPIO::Edge::FALLING, InterruptHandler, 0);

//    pins.push_back(pin);

//}
