#ifndef GPIODEVICE_H
#define GPIODEVICE_H

/*! \mainpage GPIODevice
 *
 * This project is a library.
 */


#include <stdio.h> //perror
#include <iostream>

//include library local classes
#include "QuadEncoder.h"
#include "H2Driver.h"
#include "GPIOBoard.h"



using namespace std;


class GPIODevice
{
public:
    GPIODevice();
    ~GPIODevice();

    const double& x=_x; // Interface to variable and its derivatives (read only)
    const double& dx=_dx;
    const double& ddx=_ddx;



private:



    double _x=0,_dx=0,_ddx=0; // Main variable (x) and its derivatives

    long tmperr;

};

#endif // GPIODEVICE_H
