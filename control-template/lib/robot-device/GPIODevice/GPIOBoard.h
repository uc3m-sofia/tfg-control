#ifndef GPIOBOARD_H
#define GPIOBOARD_H


#include <functional> //for function
#include <vector>

using namespace std;




class GPIOBoard
{
public:

    GPIOBoard();
//    virtual void InterruptBothEdges(long pin, void *callbackAddress ) = 0;
//    virtual void InterruptOnEdge(long pin, int (*callbackAddress)(int, char *) ) = 0;
    virtual void InterruptOnEdge(long pin, void* thisptr, function<int(int,void*)> & callback ) = 0;
//    virtual void InterruptOnRise(long pin, void* thisptr, function<int(int,void*)> & callback ) = 0;
//    virtual void InterruptOnFall(long pin, void* thisptr, function<int(int,void*)> & callback ) = 0;

protected:
//    vector<void*> pinRegister;

private:

};

#include "GPIOJetson.h"


#endif // GPIOBOARD_H
