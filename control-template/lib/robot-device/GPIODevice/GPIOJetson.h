#ifndef GPIOJETSON_H
#define GPIOJETSON_H

#include "GPIOBoard.h"
//#include "JetsonGPIO.h"

#include <vector>
#include <iostream>
#include <libsoc_gpio.h>
#include <libsoc_debug.h>
//#include <gpiod.h>??

using namespace std;
//using namespace GPIO;

#define gpioN 256

class GPIOJetson : public GPIOBoard
{
public:
    long GetPin(int pin);
    GPIOJetson();
    ~GPIOJetson();

//    void InterruptOnEdge(long pin, int (*callbackAddress)(int, char *) );
//    void InterruptBothEdges(long pin, void *callbackAddress );
//    void InterruptBothEdges(long pin, auto callbackAddress );

    void InterruptOnEdge(long pin, void* thisptr, function<int(int,void*)> & new_callback );
//    void InterruptOnRise(long pin, void* thisptr, function<int(int,void*)> & new_callback );
//    void InterruptOnFall(long pin, void* thisptr, function<int(int,void*)> & new_callback );
//    void InterruptOnEdge(long pin, void *thisptr, function<(int, void *)> &new_callback);
private:
    vector<int> pins;
    vector<gpio *> soc_gpios;
};

#endif // GPIOJETSON_H
