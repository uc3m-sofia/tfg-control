#ifndef H2DRIVER_H
#define H2DRIVER_H

#include <iostream>
#include <libsoc_gpio.h>
#include <libsoc_pwm.h>
#include <vector>

using namespace std;

class H2Driver{
public:
    H2Driver(int new_pinAIN1, int new_pinAIN2, int new_pinSTBY, int new_pinPWM);
    ~H2Driver();
    void SetPWM(double period, double duty);
    void changeDutyCycle(int duty);
    void disablePWM();
    void enablePWM();
    void setAIN1(gpio_level level);
    void setAIN2(gpio_level level);
    void enable();
    void disable();
    long SetThrottle(long nThrottle);

private:
    vector<int> pins;
    vector<gpio*> gpios;
    pwm* p;
};


#endif
