#ifndef QUADENCODER_H
#define QUADENCODER_H

#include <string>
//#include <thread>

//#include <unistd.h>
#include <functional>   // std::function, std::negate

#include <stdio.h> //perror
#include <iostream>

#include "GPIOBoard.h"

#define signalN 2

using namespace std;

class QuadEncoder
{
public:
    QuadEncoder(int new_pinA, int new_pinB, GPIOBoard & new_board );
    ~QuadEncoder();

    void EdgeOnA(int inputValue);
    void EdgeOnB(int inputValue);
    long pulses; //number of total pulses since init (can be negative)
//    QuadEncoder(int new_pinA, int new_pinB, string ioDevice = "/dev/gpiochip0");
//    ~QuadEncoder();


    //    int callbackA(int argc, char *argv);

private:

    bool ready;
//    thread tloop;

    int pins[2];
    GPIOBoard* board;
//    unsigned int pins[2];

//    gpiod_chip* chip;
//    gpiod_line_bulk line_bulk;
//    gpiod_line* line;
//    gpiod_line_bulk* lines;

    int values[signalN];
    unsigned int nlines=2;
//    gpiod_line* lineA;
//    gpiod_line* lineB; //gpiod lib input objects

    function<int(int,void*)> callbacks[4];

    int A,B; //encoder cuad signals

    int pinA,pinB;

};

#endif // QUADENCODER_H
