# - Try to find library installed at system
# Once done this will define
#  SOC_FOUND - System has the library
#  SOC_INCLUDE_DIRS - The include directories
#  SOC_LIBRARIES - The link directions

#Default location is /usr/local
set(LOCATION /usr/local)
set(NAME soc)

message(STATUS "Looking for ${NAME} in: [${LOCATION}].")

find_path(SOC_INCLUDE_DIR
    libsoc_board.h libsoc_conffile.h /libsoc_debug.h libsoc_gpio.h libsoc_i2c.h libsoc_pwm.h libsoc_spi.h
    PATHS "${LOCATION}/include/${NAME}/"
    HINTS ${LOCATION}
    PATH_SUFFIXES ${NAME} )

find_library(SOC_LIBRARY NAMES ${NAME} lib${NAME}
    PATHS "${LOCATION}/lib/${NAME}/")


if(${SOC_LIBRARY} STREQUAL "SOC_LIBRARY-NOTFOUND")
    message(STATUS "${NAME} not installed.")


else()
    message(STATUS "${NAME} library found...")

    set (SOC_FOUND TRUE)
    message(STATUS "Local ${NAME} files detected: [${SOC_LIBRARY} ].")
    message(STATUS "Local include directories: [${SOC_INCLUDE_DIR}].")
    set(SOC_LIBRARIES ${SOC_LIBRARY})
#    set(SOC_LIBRARIES ${SOC_LIBRARY} ${GPIODXX_LIBRARY} )
    set(SOC_INCLUDE_DIRS ${SOC_INCLUDE_DIR})


endif()




#mark_as_advanced(SOC_INCLUDE_DIR SOC_LIBRARY )
