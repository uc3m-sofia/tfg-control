# - Try to find library installed at system
# Once done this will define
#  GPIOD_FOUND - System has the library
#  GPIOD_INCLUDE_DIRS - The include directories
#  GPIOD_LIBRARIES - The link directions

#Default location is /usr/
set(LOCATION /usr)
set(NAME gpiod)

message(STATUS "Looking for ${NAME} in: [${LOCATION}].")

find_path(GPIOD_INCLUDE_DIR gpiod.h
    PATHS "${LOCATION}/include/${NAME}/"
    HINTS ${LOCATION}
    PATH_SUFFIXES ${NAME} )

find_library(GPIOD_LIBRARY NAMES ${NAME} lib${NAME}
    PATHS "${LOCATION}/lib/${NAME}/")

# used for c++ bindings
#find_library(GPIODXX_LIBRARY NAMES "${NAME}cxx" "lib${NAME}cxx"
#    PATHS "${LOCATION}/lib/${NAME}/")

if(${GPIOD_LIBRARY} STREQUAL "GPIOD_LIBRARY-NOTFOUND")
    message(STATUS "${NAME} not installed.")


else()
    message(STATUS "${NAME} library found...")

    set (GPIOD_FOUND TRUE)
    message(STATUS "Local ${NAME} files detected: [${GPIOD_LIBRARY} ${GPIODXX_LIBRARY}].")
    message(STATUS "Local include directories: [${GPIOD_INCLUDE_DIR}].")
    set(GPIOD_LIBRARIES ${GPIOD_LIBRARY})
#    set(GPIOD_LIBRARIES ${GPIOD_LIBRARY} ${GPIODXX_LIBRARY} )
    set(GPIOD_INCLUDE_DIRS ${GPIOD_INCLUDE_DIR})


endif()




#mark_as_advanced(GPIOD_INCLUDE_DIR GPIOD_LIBRARY )
