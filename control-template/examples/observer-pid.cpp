
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 74;  // BOARD pin 5
const int pin_B = 216; // BOARD pin 7

const int pin_ain1 = 149;  // BOARD pin 29
const int pin_ain2 = 200; // BOARD pin 31
const int pin_stby = 13;   // BOARD pin 27
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
#include "fcontrol.h"
#include "IPlot.h"



//void (*const blinkPtr)(int);

using namespace std;

int main()
{

    double dts=0.01;
    int pulsos_anterior;
    int pulsos_actual=0;
    long velocidad;
    GPIOJetson sbc;

    QuadEncoder enc(pin_A,pin_B,sbc);
    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);

//    vector<vector<double>> G={ {1,0.0089,0.0043} , {0,0.7864,0.8671} , {0,-5.8183E-4,-0.9975} };
    vector<vector<double>> G={ {1,0.0089,0.0077} , {0,0.7865,1.5351} , {0,-0.0006,-0.9956} };
//    vector<vector<double>> H={ {0.3049} , {60.9740} , {0.1740} };
    vector<vector<double>> H={ {0.3046} , {60.9158} , {0.1739} };
    vector<vector<double>> C={ {1,0,0} };
    vector<vector<double>> D={ {0} };


    StateObserver obs(G,H,C,D,dts);
//    obs.SetGainsK(vector<vector<double>> { {-1.20916790564601/10} , {183298.409524/10} , {-377952.56907978/10} });
    obs.SetGainsK(vector<vector<double>> { {0.01} , {0.1} , {1} });

    vector<vector<double>> x={ {0} , {0} , {0} };

//    PIDBlock CPID(0.0060,0.2696,0.0,dts); //vel loop w=50, pm=90
    PIDBlock CPID(0.0137,0.6276,0.0,dts); //vel loop w=100, pm=90


    drv.enable();
    drv.SetThrottle(50);


    SamplingTime Ts(dts); // en segundos

    PIDBlock C1(1,0,0,dts);
    vector<double> num = {0,1};
    vector<double> den = {-1,1};
    SystemBlock P1(num,den);

    IPlot plot1(dts,"title"," x axis","y axis");
    IPlot plot2(dts,"velocity"," time (s)"," vel(rad/s)");

    double out=0,u=0;
    double err=0;
    double ref=20;

    for (int i=0;i<500;i++)
    {
        err=ref-velocidad;

//        cout << "currently at pulse: " << enc.pulses << endl;
        out=P1.OutputUpdate(1);
        u= CPID.OutputUpdate(err);
        drv.SetThrottle(u*20);

        obs.Update(u, 0.2244*enc.pulses);
        x=obs.GetState();
        velocidad=x[1][0];

//        pulsos_anterior=pulsos_actual;
//        pulsos_actual=enc.pulses;
//        velocidad=((pulsos_actual-pulsos_anterior)*2*M_PI)/(28*dts);

        plot1.pushBack(CPID.GetState());
        plot2.pushBack(velocidad);


        Ts.WaitSamplingTime();
    }
    drv.SetThrottle(0);

    plot1.Plot();
    plot2.Plot();


    cout << "enc stopped at pulse: " << enc.pulses << endl;






    return 0;
}
