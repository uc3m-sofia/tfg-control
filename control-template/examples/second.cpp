#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 74;  // BOARD pin 5
const int pin_B = 216; // BOARD pin 7

const int pin_ain1 = 149;  // BOARD pin 29
const int pin_ain2 = 200; // BOARD pin 31
const int pin_stby = 13;   // BOARD pin 27
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
#include "fcontrol.h"
#include "IPlot.h"



//void (*const blinkPtr)(int);

using namespace std;
//void (*const blinkPtr)(int);
using namespace std;

int main()
{
    double dts=0.005;
    int pulsos_anterior;
    int pulsos_actual=0;
    int velocidad;
    GPIOJetson sbc;
    QuadEncoder enc(pin_A,pin_B,sbc);
    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);
    SamplingTime Ts(dts); // en segundos
    PIDBlock C1(1,0,0,dts);
    vector<double> num = {0,1};
    vector<double> den = {-1,1};
    SystemBlock P1(num,den);
    IPlot plot1(dts,"title"," x axis","y axis");
    double out=0;
    for (int j=0;j<=1;j++)
    {
        cout<<"J =" << j <<endl;
        if(j%2==0)
            {
                drv.enable();
                drv.SetThrottle(100);
             }
        else
            {
            drv.SetThrottle(0);
            }
         for (int i=0;i<100;i++)
             {
             pulsos_anterior=pulsos_actual;
             pulsos_actual=enc.pulses;
             velocidad=((pulsos_actual-pulsos_anterior)*60)/(28*dts);
             cout<<velocidad<<endl;
             plot1.pushBack(velocidad);
             Ts.WaitSamplingTime();
                      }
    }
    drv.SetThrottle(0);
    plot1.Plot();
    cout << "enc stopped at pulse: " << enc.pulses << endl;
    return 0;
}
