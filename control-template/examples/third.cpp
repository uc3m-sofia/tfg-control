#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 74;  // BOARD pin 5
const int pin_B = 216; // BOARD pin 7

const int pin_ain1 = 149;  // BOARD pin 29
const int pin_ain2 = 200; // BOARD pin 31
const int pin_stby = 13;   // BOARD pin 27
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
#include "fcontrol.h"
#include "IPlot.h"



//void (*const blinkPtr)(int);

using namespace std;

int main()
{
    double dts=0.01;
    int pulsos_anterior;
    int pulsos_actual=0;
    double velocidad;

    GPIOJetson sbc;

    QuadEncoder enc(pin_A,pin_B,sbc);
    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);

    drv.enable();
    drv.SetThrottle(100); //Throttle al 100 %, máximo voltage de entrada que
                          //puede dar el driver del motor DC

    SamplingTime Ts(dts); // Ts=0.01 segundos

    IPlot plot1(dts,"title"," x axis","y axis");

    for (int i=0;i<200;i++)
    {

        pulsos_anterior=pulsos_actual;

        pulsos_actual=enc.pulses; //leemos y guardamos en la variable la cantidad de pulsos
                                  //leidos por el encoder hasta este punto del código

        velocidad=((pulsos_actual-pulsos_anterior)*60)/(28*dts); // La dfirencia entre los pulsos
                                  //leidos por el encoder en la útlima iteración, menos los leídos
                                  //en la penúltima iteración, divida por la diferencia de tiempo
                                  //entre iteraciones, nos da la velocidad del motor, en pulsos/segundos.
                                  //Multiplicando la velocidad por 60 y dividiendola por 28 obtenemos la
                                  //velocidad en rpm.

        plot1.pushBack(velocidad); //guardamos los valores de velocidad de cada iteración en un vector.
        cout<<velocidad<<endl;
        Ts.WaitSamplingTime(); //El sistema espera Ts, es decir 0.01 s,
                               //por lo tanto cada iteración del bucle durara
                               // 0.01 s completandose así el bucle en 2 segundos
    }
    drv.SetThrottle(0); //Detenemos la entrada de voltage al motor.

    plot1.Plot();       //Imprimimos los valores de velocidad por iteración en una gráfica.

    cout << "enc stopped at pulse: " << enc.pulses << endl;
    return 0;
}
