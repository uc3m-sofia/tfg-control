% myAdd(0:99,0:99)

% jet1=jetson('192.168.1.39','sofia','sofia');

cfg = coder.gpuConfig('exe');
cfg.Hardware = coder.hardware('NVIDIA Jetson');
cfg.Hardware.BuildDir = '~/remoteBuildDir';
cfg.CustomSource  = fullfile('main.cu');

codegen('-config ',cfg,'myAdd','-args',{1:100,1:100});