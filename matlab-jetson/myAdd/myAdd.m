function out = myAdd(inp1,inp2) %#codegen

coder.gpu.kernelfun();
out = inp1 + inp2;

hwobj = jetson;
pinvalue = readDigitalPin(hwobj,7);
end