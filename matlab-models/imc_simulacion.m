
clear;close all;%#ok<*NOPTS>

k=0.01; %V/rad/sec or 0.01 N.m/Amp
b=0.11; %Nms
J=0.01; %kg/m^2
L=0.5; %H
r=1; %Ohm

dts=0.1;
z=tf('z',dts);

%planta
A = [-b/J   k/J
    -k/L   -r/L];
B = [0
    1/L];
C = [1 0];
D = 0;

motor_ss = ss(A,B,C,D);
motor_zz= c2d (motor_ss,dts);

[G, H, C, D] = ssdata(motor_zz);

sys=tf(motor_zz);

%modelo 
k=0.01; %V/rad/sec or 0.01 N.m/Amp 
b=0.11; %Nms 
J=0.02; %kg/m^2 
L=0.5; %H 
r=1; %Ohm 


A = [-b/J   k/J
    -k/L   -r/L];
B = [0
    1/L];
C = [1 0];
D = 0;


motor_ss = ss(A,B,C,D);
motor_zz= c2d (motor_ss,dts);

[G, H, C, D] = ssdata(motor_zz);

mod=tf(motor_zz);
modelo=tf(motor_ss);

N=100;
t=dts*(0:N-1);
y=zeros(1,N);
x=zeros(2,N);


filtro=tf(1,[0.25 1 1]); %filtro grado 2
filtro=c2d(filtro,dts);
Q=minreal(filtro/tf(mod));
Qg=1; %controller extra gain;
Q=tf(Q*Qg);

xq=zeros(2,N);
os=zeros(1,N);
oq=zeros(1,N);
om=zeros(1,N);
e=zeros(1,N);

Ns=sys.Numerator{1};
Ns=Ns(find(Ns)); %remove zero values
Ds=sys.Denominator{1};
Ds(1)=[];

Nq=Q.Numerator{1}
Nq=Nq(find(Nq)); %remove zero values
Dq=Q.Denominator{1}
Dq(1)=[];

Nm=mod.Numerator{1}
Nm=Nm(find(Nm)); %remove zero values
Dm=mod.Denominator{1}
Dm(1)=[];

Qord=size(Dq,2);
Sord=size(Ds,2);

uq=0*Nq; yq=0*Dq;
us=0*Ns; ys=0*Ds;
um=0*Nm; ym=0*Dm;

r=1;

for k=1:N

    oq(k)= -Dq*yq' + Nq*uq';
    os(k)= -Ds*ys' + Ns*us';  
    om(k)= -Dm*ym' + Nm*um';

    %update input vector values
    um=circshift(um,1);
    uq=circshift(uq,1);
    us=circshift(us,1);

    %update output vector values
    ys=circshift(ys,1);
    ys(1)=os(k);
    yq=circshift(yq,1);
    yq(1)=oq(k);
    ym=circshift(ym,1);
    ym(1)=om(k);

    %imc
    e(k)=r-(os(k)-om(k));
    uq(1)=e(k); us(1)=oq(k); um(1)=oq(k); 

end


figure;
hold on;
plot(t,os,'r');
step(filtro);
plot(t,om,'g');

legend('sys', 'filtro', 'mod');
%legend('sys', 'mod');
%legend('sys', 'filtro');


