clear;close all;%#ok<*NOPTS>

% parameters
clear;close all;
% pkg load control
%kt=ke=k  
k=0.0033515; %V/rad/sec or 0.01 N.m/Amp
b=4.7047e-08; %Nms
J=4.7711e-08; %kg/m^2
L=0.0000711; %H
r=10.3; %Ohm

dts=0.01;

A = [0 1 0
    0 -b/J   k/J
    0 -k/L   -r/L];
B = [0
    0
    1/L];
C = [0 1 0];
D = 0;

% motor_ss = canon( ss(A,B,C,D) ,'companion');
motor_ss = ss(A,B,C,D);
motor_zz= c2d (motor_ss,dts,'tustin');

[G, H, C, D] = ssdata(motor_zz);
% G=G';M=C;C=H';H=C';D=D;

%%
N=250
y=zeros(1,N);
e=zeros(1,N);
r=zeros(1,N);
aux=0;
aux2=0;
for c=1:N
    
    if(aux==50)
        aux=0;
        if(r(c-1)==0)
            aux2=5.13;
        else 
            aux2=0;
        end
    end
    r(c)=aux2;
    aux=aux+1
end
x=zeros(3,N);
x(:,1)=[0;0;0];


for k=1:N-1

    e(k)=r(k+1);
    y(k)=(0.10472)^-1*C*x(:,k);
    x(:,k+1)=G*x(:,k) + H*e(k);



end
    y(N)=(0.10472)^-1*C*x(:,N);


% hold on;
figure;
plot(x');
legend('estx1', 'estx2', 'estx3');


figure;
plot(y);

hold on

plot(r);


