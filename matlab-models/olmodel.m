clear;close all;%#ok<*NOPTS>

% parameters
clear;close all;
% pkg load control
%kt=ke=k  

% - Va  = 5'13 V
% - Ia = 0,037 A
% - W = 13499 rpm = 1413' 615 rad/s
% - tm (Cnste de tiempo mecánica) = 0'0435446 s
% - Iarr (Intensidad de arranque) = 0'0172 A

J=4.7711E-08  %Kg.m^2
L=0.126E-3; %H
b = 4.7054E-08 %N.m.s
k = 0.0033594  %V/rad s  (297,67220337 rad/V·s / 1870,329614568 rev/V·s)
r  = 10.3 %Ohms

dts=0.01;

A = [0 1 0
    0 -b/J   k/J
    0 -k/L   -r/L];
B = [0
    0
    1/L];
C = [1 0  0];
D = 0;

% motor_ss = canon( ss(A,B,C,D) ,'companion');
motor_ss = ss(A,B,C,D);
motor_zz= c2d (motor_ss,dts,'tustin');

[G, H, C, D] = ssdata(motor_zz);
% G=G';M=C;C=H';H=C';D=D;

p1=0.5+0.5*1i;
p2=conj(p1);
p3=0.5;

% step(motor_zz);
step(zpk([],[p1 p2 p3],1,dts))
% end parameters

Mo=[C;C*G;C*G^2]
N=rank(Mo)
Mc=[C;C*G;C*G^2]
N=rank(Mo)



N=1000
y=zeros(1,N);
e=zeros(1,N);
r=1;
x=zeros(3,N);
x(:,1)=[0;0;0];
u=1;

% hold on;
for k=1:N-1

    y(k)=C*x(:,k);
    x(:,k+1)=G*x(:,k) + H*u;

%     x=G*x + H*(K*x+tgt);

end
    y(N)=C*x(:,N);


% hold on;
figure;
plot(x');
legend('estx1', 'estx2', 'estx3');


figure;
% plot(x(3,:));


