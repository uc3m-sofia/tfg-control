check deps:

sudo apt install autoconf build-essential libtool-bin pkg-config python-dev python3-dev

Install libsoc from: https://github.com/jackmitch/libsoc

get template form gitlab.

Configure PWM outputs according to :
https://docs.nvidia.com/jetson/archives/r34.1/DeveloperGuide/text/HR/ConfiguringTheJetsonExpansionHeaders.html


sudo /opt/nvidia/jetson-io/jetson-io.py

If not working use:

sudo cp -v /boot/tegra210-p3448-0000-p3449-0000-[ab]0[012].dtb /boot/dtb/

and run again

sudo /opt/nvidia/jetson-io/jetson-io.py


